<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-LRlmVvLKVApDVGuspQFnRQJjkv0P7/YFrw84YYQtmYG4nK8c+M+NlmYDCv0rKWpG" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400|Roboto" rel="stylesheet">

    <link rel="stylesheet" href="assets/css/master.css">

    <script src="assets/js/lib/jquery.min.js"></script>
    <script src="assets/js/main.js"></script>

    <title>Kingsen</title>
</head>


<?php
session_start();
if(!isset($_SESSION["lobbyCode"]) || !isset($_SESSION["playerName"])){
//    $_SESSION["lobbyCode"] = "235OH";
//    $_SESSION["playerName"] = "sandertjee";
}

//echo $_SESSION["lobbyCode"];
//echo $_SESSION["playerName"];
?>
<link rel="stylesheet" type="text/css" href="index.css">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

<div id="title">
   <img src="assets/media/Kingsen-logo-w.png"/>
</div>

<div id="lobby">
    <div id="players-container">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script>


            var BASE_URL = "https://sandervanderburgt.com/kingsen_official/root/";

            var playerInfoResult = "";

            setInterval(function(){
                $.get( BASE_URL + "/api/?type=lobby_get_players", function( data ) {
                    var jsonResult = JSON.parse(data);



                    document.getElementById("players-container").innerHTML = "";
                    for (var x in jsonResult) {

                        $.get( BASE_URL + "/api/?type=get_player_info&value="+ jsonResult[x], function( data ) {
                            playerInfoResult = JSON.parse(data);


                            if(playerInfoResult["player_name"] === jsonResult[x]){
                                document.getElementById("players-container").innerHTML += "<div class=\"player-container\">" +
                                    jsonResult[x] +
                                    "YOU" +
                                    "</div>";
                            }
                            else{
                                document.getElementById("players-container").innerHTML += "<div class=\"player-container\">" +
                                    jsonResult[x] +
                                    "Not you" +
                                    "</div>";
                            }

                        });








                    }

                });
            }, 1000)


        </script>
    </div>
</div>