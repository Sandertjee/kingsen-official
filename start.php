<div id="start">
    <form>
        <input type="text" id="createLobbyPlayerName" placeholder="Name"/>
        <input type="button" id="createLobby" value="Create Lobby"/>
    </form>

    <hr/>
    <form>
        <h2>Join game:</h2>
        <input type="text" id="joinLobbyCode" placeholder="Lobby code" maxlength="5" style="text-transform:uppercase"/>
        <input type="text" id="joinLobbyUsername" placeholder="Username"/>
        <input type="button" id="joinLobby" value="Join game">
    </form>




    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <?php
    session_start();
    ?>
    <script>

        var BASE_URL = "https://sandervanderburgt.com/kingsen_official/root/";

        // if the create lobby button is clicked
        $("#createLobby").on("click", function(){
            // check if field is filled in
            if($("#createLobbyPlayerName").val() === ""){
                // alert user
                alert("please fill in all fields!")
            }
            else{
                // genreate the room code
                var playerName = document.getElementById("createLobbyPlayerName").value;
                createLobby(playerName);
            }
        });

        $("#joinLobby").on("click", function(){
            // check if field is filled in
            var lobbyCode = $("#joinLobbyCode").val();
            var lobbyPlayerName = $("#joinLobbyUsername").val();


            if(lobbyCode === ""){
                // alert user
                alert("please fill in all fields!")
            }
            else if(lobbyCode.length > 5){
                alert("Invalid lobby code!")
            }
            else if(lobbyPlayerName === ""){
                // alert user
                alert("please fill in all fields!")
            }
            else{
                joinLobby(lobbyPlayerName, lobbyCode);
            }
        });


        function createLobby(playerName){
            // create lobby using API
            $.get(BASE_URL + "/api/?type=lobby_new&value=" + playerName)
                .done(function() {
                    // Exists
                    console.log("Room created");
                    return true;
                }).fail(function() {
                // Something went wrong apparently
                console.log("Something went wrong when creating lobby!");
                return false;
            });
        }

        function joinLobby(playerName, lobbyCode){
            // Join lobby using API
            $.get(BASE_URL + "/api/?type=player_join&value=" + lobbyCode + "," + playerName, function( data ){

                if(data !== ""){
                    var jsonData = JSON.parse(data);
                    if(jsonData["error"]){
                        alert(jsonData["message"]);
                    }
                }
            });
        }


        getrandomcard();

        function getrandomcard(playerName, lobbyCode){
            // Join lobby using API
            $.get(BASE_URL + "/api/?type=card_random", function( data ){

                console.log(data);
            });
        }



        // function createLobby2(lobbyCode){
        //     // get playername
        //     var playerName = document.getElementById("createLobbyPlayerName").value;
        //
        //
        //
        //     var playerData = [playerName, lobbyCode];
        //
        //     setTimeout(function(){
        //         // generate new player when room is created
        //         $.get(BASE_URL + "api/?type=player&action=new&value=" + playerData)
        //             .done(function() {
        //                 // Exists
        //                 console.log("Created player " + playerName);
        //                 return true;
        //             }).fail(function() {
        //             // Something went wrong apparently
        //             console.log("Something went wrong creating user!");
        //             return false;
        //         });
        //     }, 1000);
        // }

        // function roomExists(lobbyCode, d, dpm, f, fpm){
        //     $.get(BASE_URL + "rooms/" + lobbyCode)
        //         .done(function() {
        //             // Exists
        //             log("Room does exists!");
        //             d(dpm);
        //         }).fail(function() {
        //         log("Room doesn't exist!");
        //             f(fpm);
        //         return false;
        //     });
        // }

        // function joinPlayer(playerData){
        //
        //     playerName = playerData[0];
        //     lobbyCode = playerData[1];
        //
        //     $.get(BASE_URL + "api/?type=player&action=new&value=" + playerData)
        //         .done(function() {
        //             // Exists
        //             console.log("Player " + playerName + " Joined Lobby " + lobbyCode);
        //             return true;
        //         }).fail(function() {
        //         // Something went wrong apparently
        //         console.log("Something went wrong joining player to lobby!");
        //         return false;
        //     });
        // }


    </script>
</div>


<?php


