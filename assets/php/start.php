<?php
    session_start();
?>
    <div id="start" class="page">
        <form>
            <img src="assets/media/Kingsen-logo-b.png" alt="Kingsen">
            <div class="fields">
                <label for="joinLobbyCode">Lobby code</label>
                <input type="text" id="joinLobbyCode" placeholder="Lobby code" maxlength="5" style="text-transform:uppercase"/>
                <label for="joinLobbyCode" id="labelLobbyCode" class="helper"></label>

                <label for="joinLobbyUsername">Spelersnaam</label>
                <input type="text" id="joinLobbyUsername" placeholder="Spelersnaam"/>
                <label for="joinLobbyUsername" id="labelUsername" class="helper"></label>
            </div>
            <div class="submit">
                <button type="button" id="joinLobby">Spelen</button>
                <a href="#" id="create">Nieuwe lobby maken</a>
            </div>
        </form>
        <script src="assets/js/start.js"></script>
    </div>
<?php


