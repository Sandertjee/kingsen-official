<?php
    session_start();
    $date = date("dmYHis");
    require_once "../../api/classes/Lobby.php";
    require_once "../../api/classes/Player.php";
?>
<div id="game" class="page">
    <div id="player_bar">
        <h1><?=Player::getSessionName();?></h1>
        <p><?=Lobby::getSessionCode();?></p>
    </div>
    <div id="card_table">
        <div id="card" class="card" data-view="front">
            <div class="side back">
                <div class="inner" style="background-image: url('assets/media/kingsen-back.png?v=<?=$date;?>')"></div>
            </div>
            <div class="side front black">
                <div class="inner">
                    <div class="identifier">
                        <i class="suit fas fa-square"></i>
                        <span class="id">0</span>
                    </div>
                    <div class="middle">
                        <i class="icon fal fa-circle"></i>
                        <h3>None</h3>
                    </div>
                    <div class="identifier bottom">
                        <i class="suit fas fa-square"></i>
                        <span class="id">0</span>
                    </div>
                </div>
            </div>
        </div>
        <div id="card-back" class="card" data-view="front">
            <div class="side back">
                <div class="inner" style="background-image: url('assets/media/kingsen-back.png?v=<?=$date;?>')"></div>
            </div>
        </div>
    </div>
    <script src="assets/js/lib/hammer.min.js"></script>
    <script src="assets/js/Card.js?v=<?=$date;?>"></script>
    <script src="assets/js/game.js?v=<?=$date;?>"></script>
</div>