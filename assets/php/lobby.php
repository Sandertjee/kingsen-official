<?php
    session_start();
    require_once "../../api/classes/Lobby.php";
    require_once "../../api/classes/Player.php";
?>
<div id="lobby" class="page">
    <div id="player_bar">
        <h1><?=Player::getSessionName();?></h1>
        <p><?=Lobby::getSessionCode();?></p>
    </div>
    <div id="players-container"></div>
    <script src="assets/js/lobby.js"></script>
</div>


