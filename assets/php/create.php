<?php
    session_start();
?>
    <div id="start" class="page">
        <form>
            <img src="assets/media/Kingsen-logo-b.png" alt="Kingsen">
            <div class="fields">
                <label for="createLobbyPlayerName">Spelersnaam</label>
                <input type="text" id="createLobbyPlayerName" placeholder="Spelersnaam"/>
                <label for="createLobbyPlayerName" id="labelCreateLobbyPlayerName" class="helper"></label>
            </div>
            <div class="submit">
                <button type="button" id="createLobby">Create Lobby</button>
                <a href="#" id="back">Terug</a>
            </div>
        </form>
        <script src="assets/js/start.js"></script>
    </div>
<?php


