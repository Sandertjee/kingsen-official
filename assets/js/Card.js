// Card function / class
var LEFT = 2;
var RIGHT = 4;
var UP = 8;
var DOWN = 16;

function initCard(main_elem) {

    // Vars
    var card_$ = $(main_elem);
    var card_back = card_$.parent().children("#card-back");
    var card_elem = document.querySelector(main_elem);
    var hitbox = card_elem.parentElement;
    var overlay = card_$.parent();
    var screen_width = overlay.width();
    var card_width = card_$.width();
    var screen_height = overlay.height();
    var card_height = card_$.height();
    var is_clickable = true;
    var is_swipeable = true;
    var is_front = true;

    // On loaded, reset box to grey color
    resetElement();

    // Create manager to add multiple events
    var mc = new Hammer.Manager(hitbox);
    mc.add(new Hammer.Pan({threshold: 0, pointers: 0}));
    mc.add(new Hammer.Swipe()).recognizeWith(mc.get('pan'));

    // Add the Swipe event
    mc.on("swipe", onSwipe);
    card_$.on('click', function () {
        flipCard(750);
    });

    // Place card in the middle of the screen and reset values
    function resetElement() {
        card_$.animate({deg: 10}, {
            duration: 0,
            step: function () {
                card_$.css({
                    top: (screen_height / 2) - (card_height / 2),
                    left: (screen_width / 2) - (card_width / 2),
                    width: "212px",
                    height: "300px"
                })
            }
        });
        card_back.css({
            top: (screen_height / 2) - (card_height / 2),
            left: (screen_width / 2) - (card_width / 2),
            width: "212px",
            height: "300px"
        });
        console.log(card_back);
        console.log("Card reset");
        flipAnimation(card_$, 0, true);
    }

    // Body on swipe direction switch
    function onSwipe(ev) {
        switch (ev.direction) {
            case LEFT:
                onSwipeLeft(ev);
                break;
            case RIGHT:
                onSwipeRight(ev);
                break;
            case UP:
                onSwipeUp(ev);
                break;
            case DOWN:
                onSwipeDown(ev);
                break;
        }
    }

    function onSwipeLeft(ev) {}

    function onSwipeRight(ev) {
        if (is_swipeable){
            slideAnimationOut(card_$);
        }
    }

    function onSwipeUp(ev) {}

    function onSwipeDown(ev) {}

    // The flip card animation
    function flipCard(flip_time) {
        if (is_clickable) {
            if (card_$.attr('data-view') !== 'front') {
                // Flip to front
                flipAnimation(card_$, flip_time, false);
                $.ajax({
                    type: "GET",
                    dataType: 'application/json',
                    url: "api/?type=card_random",
                    success: function (result) {
                        console.log(result);
                        var json_parse = JSON.parse(result);
                        setCardValues(card_$, json_parse.color, json_parse.suit, json_parse.symbol, json_parse.icon, json_parse.title);
                    },
                    error: function (result) {
                        if (result.responseText[0] === "{") {
                            var json_parse = JSON.parse(result.responseText);
                            setCardValues(card_$, json_parse.color, json_parse.suit, json_parse.symbol, json_parse.icon, json_parse.title);
                        }
                    }
                });
                console.log("Flip to front")
            }
            is_clickable = false;
            setTimeout(function () {
                is_clickable = true;
            }, flip_time);
        }
    }

    // Global function for setting values in the card
    function setCardValues(card, color, suit, symbol, icon, title) {
        var front = card_$.find('.side.front');
        var symbol_elem = card.find(".identifier .id");
        var suit_elem = card.find(".identifier .suit");
        var icon_elem = card.find(".middle .icon");
        var title_elem = card.find(".middle h3");

        symbol_elem.html(symbol);
        suit_elem.attr("class", "suit fas fa-" + suit);
        icon_elem.attr("class", "icon fal fa-" + icon);
        front.attr("class", "front side " + color);
        title_elem.html(title);
    }

    function flipAnimation(card, flip_time, flipped_back) {
        var front = card_$.find('.side.front');
        var back = card_$.find('.side.back');
        var flip_to = 0;
        var front_z = 10;
        var back_z = 10;
        var card_x = 0;

        if (flipped_back) {
            // Flip to back
            card.attr('data-view', 'back');
            flip_to = 180;
            front_z--;
            back_z++;
            card_x = (screen_width / 2) - (card_width / 2);
            is_swipeable = false;
        } else {
            // Flip to front
            card.attr('data-view', 'front');
            flip_to = 0;
            front_z++;
            back_z--;
            card_x = -400;
            is_swipeable = true;
        }
        card.animate({deg: flip_to}, {
            duration: flip_time,
            step: function (now) {
                card.css({
                    top: (screen_height / 2) - (card_height / 2),
                    left: (screen_width / 2) - (card_width / 2) + (card_width * 1.5)
                });
                front.css({
                    transform: 'rotateY(' + now + 'deg)'
                });
                back.css({
                    transform: 'rotateY(' + (now * -1) + 'deg)'
                });
            }
        });

        setTimeout(function () {
            front.css("z-index", front_z);
            back.css("z-index", back_z);
            card.animate({deg: flip_to}, {
                duration: flip_time,
                step: function (now) {
                    card.css({
                        top: (screen_height / 2) - (card_height / 2),
                        left: (screen_width / 2) - (card_width / 2),
                        display: "block"
                    });
                }
            });

        }, (flip_time / 2));
    }

    function slideAnimationOut(card) {
        card.css({
            left: "100%"
        });
        setTimeout(function () {
            card.css({
                display: "none"
            });
        }, 1500);
        setTimeout(function () {
            resetElement();
        }, 2000);
    }

    console.log("Card loaded");
}