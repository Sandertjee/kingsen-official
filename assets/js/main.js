// Global vars
var STATE_LOGIN = 0;
var STATE_CREATE = 1;
var STATE_LOBBY = 2;
var STATE_GAME = 3;
var STATE_END = 4;
var STATE_CURRENT = 99;

var player = null;
var lobby = null;
var canvas = null;

var state_override = true;
var state = 99;
var game_data = null;
var player_count = 0;

// Init when page loads
$(document).ready(function () {
    canvas = $("#canvas");
    initGame();
    console.log("Kingsen started");
});

function initGame() {
    single();
    // Repeater was set in a interval to get the data (realtime)
    setInterval(repeater, 1000);
    // Also init Repeater when page loads
    repeater();
}

function single() {
    // Override for when a lobby hasn't created of joined
    if (state_override){
        STATE_CURRENT = STATE_LOGIN;
    }
}

function repeater() {
    var the_state = 0;
    // Get global game data for page content
    $.getJSON( "api/?type=game_get_data", function( data ) {
        console.log("State: " + state + " ~ Current: " + STATE_CURRENT + " ~ override: " + state_override);
        if (STATE_CURRENT !== state){
            if (state_override){
                the_state = STATE_CURRENT;
            } else {
                the_state = state;
            }
            setPageContent(the_state);
        }
        setData(data);
    });
}

// Determine the page content on stage
function setPageContent(the_state) {
    switch (the_state){
        case STATE_LOGIN:
            loadState('start.php');
            STATE_CURRENT = STATE_LOGIN;
            state_override = true;
            break;
        case STATE_CREATE:
            loadState('create.php');
            STATE_CURRENT = STATE_CREATE;
            state_override = true;
            break;
        case STATE_LOBBY:
            loadState('lobby.php');
            STATE_CURRENT = STATE_LOBBY;
            state_override = false;
            break;
        case STATE_GAME:
            loadState('game.php');
            STATE_CURRENT = STATE_GAME;
            state_override = false;
            break;
        case STATE_END:
            STATE_CURRENT = STATE_END;
            break;
        default:
            console.log("Kingsen started");
            // STATE_CURRENT = STATE_LOGIN;
            break;
    }
}

// Get file from php folder in assets
function loadState(file_name) {
    var file_dir = "assets/php/" + file_name;
    // console.log(file_dir);
    canvas.html("");
    canvas.load(file_dir);
}

// Get data from JSON string and fill vars
function setData(data) {
    if (!state_override || typeof data.lobby_code !== 'undefined'){
        state_override = false;
        state = data.game_state;
        game_data = data;
    } else {
        state = STATE_CURRENT;
    }
    // console.log(data);
}
console.log("Booting up Kingsen...");