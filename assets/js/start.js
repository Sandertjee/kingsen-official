$(document).ready(function () {
    var inputJoinLobbyCode = $("#joinLobbyCode");
    var labelJoinLobbyCode = $("#labelLobbyCode");

    var inputJoinUsername = $("#joinLobbyUsername");
    var labelJoinUsername = $("#labelUsername");

    var inputCreateUsername = $("#createLobbyPlayerName");
    var labelCreateUsername = $("#labelCreateLobbyPlayerName");

    var buttonCreateLobby = $("#createLobby");
    var buttonJoinLobby = $("#joinLobby");

    // if the create lobby button is clicked
    buttonCreateLobby.on("click", function () {
        // check if field is filled in
        if (inputCreateUsername.val() === "") {
            // alert user
            labelCreateUsername.html("please fill in all fields!");
        }
        else {
            // generate the lobby
            var playerName = document.getElementById("createLobbyPlayerName").value;
            labelCreateUsername.html("");
            createLobby(playerName);
        }
    });

    buttonJoinLobby.on("click", function () {
        // check if field is filled in
        var lobbyCode = inputJoinLobbyCode.val();
        var lobbyPlayerName = inputJoinUsername.val();


        if (lobbyCode === "") {
            // alert user
            labelJoinLobbyCode.html("please fill in all fields!");
            labelJoinUsername.html("please fill in all fields!");
        }
        else if (lobbyCode.length > 5) {
            labelJoinLobbyCode.html("Invalid lobby code!")
        }
        else if (lobbyPlayerName === "") {
            // alert user
            labelJoinUsername.html("please fill in all fields!")
        }
        else {
            labelJoinLobbyCode.html("");
            labelJoinUsername.html("");
            joinLobby(lobbyPlayerName, lobbyCode);
        }
    });

    // if the create lobby button is clicked
    $("#create").bind("click", function (e) {
        e.preventDefault();
        setPageContent(STATE_CREATE);
    });
    // if the back button is clicked
    $("#back").bind("click", function (e) {
        e.preventDefault();
        setPageContent(STATE_LOGIN);
    });

    function createLobby(playerName) {
        // create lobby using API
        $.get("api/?type=lobby_new&value=" + playerName)
            .done(function () {
                // Exists
                labelCreateUsername.html("Room created");
                setPageContent(STATE_LOBBY);
                return true;
            }).fail(function () {
            // Something went wrong apparently...
            labelCreateUsername.html("Something went wrong when creating lobby!");
            return false;
        });
    }

    function joinLobby(playerName, lobbyCode) {
        // Join lobby using API
        $.get("api/?type=player_join&value=" + lobbyCode + "," + playerName, function (data) {
            if (data !== "") {
                var jsonData = JSON.parse(data);
                if (jsonData["error"]) {
                    // Something went wrong apparently...
                    labelJoinLobbyCode.html(jsonData["message"]);
                } else {
                    // Everything is fine
                    setPageContent(STATE_LOBBY);
                }
            }
        });
    }

    //
    //
    // getrandomcard();
    //
    // function getrandomcard(playerName, lobbyCode) {
    //     // Join lobby using API
    //     $.get("api/?type=card_random", function (data) {
    //
    //         console.log(data);
    //     });
    // }
});