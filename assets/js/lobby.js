var playerInfoResult = "";
var player_results = [];
var your_name = null;
var to_game = false;

setInterval(function () {

    // Get JSON string with players in lobby
    $.get("api/?type=lobby_get_players", function (data) {
        var jsonResult = JSON.parse(data);
        // document.getElementById("players-container").innerHTML = "";
        var count = 0;
        for (var x in jsonResult) {
            // Retrieve JSON string with data from player
            $.get("api/?type=get_player_info&value=" + jsonResult[x], function (data) {
                playerInfoResult = JSON.parse(data);
                player_results[count] = playerInfoResult;

                // Set player name as separate var for comparing in visuals below
                if (game_data.player_display_name === playerInfoResult["player_name"]) {
                    your_name = playerInfoResult["player_name"];
                }
                count++;
            });
        }
    });
    player_results.sort();
    console.log(player_results);
    console.log(player_results.length + " players ~ show players " + player_count);
    if (!to_game && (player_count < player_results.length || player_count > player_results.length)) {

        // console.log("Hello there");
        player_count = player_results.length;
        document.getElementById("players-container").innerHTML = "";

        for (var i = 0; i < player_results.length; i++) {
            var card_player;

            // Check if player is U
            if (player_results[i].player_name === your_name) {
                card_player = {name: player_results[i].player_name, icon: "user-crown"};
            } else {
                card_player = {name: player_results[i].player_name, icon: "user"};
            }
            document.getElementById("players-container").innerHTML += "<div class=\"player-card\">" +
                "<i class=\"icon fal fa-" + card_player.icon +"\"></i>" +
                "<span class=\"name\">" + card_player.name + "</span>" +
                "</i>";
            console.log(player_results[i]);
        }
        // console.log(show_player_results);
    } else if (player_count === 3) {
        player_count = 500;
        $.get("api/?type=lobby_next_game_state", function () {
            if (!to_game){
                to_game = true;
                setTimeout(function () {
                    setPageContent(STATE_GAME);
                }, 2000);
            }
        });
    } else {
        console.log("No changes");
        console.log(your_name);
    }
}, 1000);

