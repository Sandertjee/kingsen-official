<?php
/**
 * Created by PhpStorm.
 * User: Sander
 * Date: 12-12-2018
 * Time: 12:38
 */

class Card {
    public function __construct($action, $value) {
        switch($action){
        }
    }

    static function generateCards(){
        $fileEditor = new FileEditor(__DIR__ . "/../../assets/json/");
        $cards = [];
        $json = json_decode($fileEditor->getFile("cards", "json"), true);


        $counter = 1;
        foreach($json["suits"] as $suit => $suitValues){
            foreach($json["cards"] as $card){
                $cards[$counter] = [
                    "id" => $counter,
                    "symbol" => $card["symbol"],
                    "suit" => $suitValues["type"],
                    "color" => $suitValues["color"],
                    "icon" => $card["icon"],
                    "title" => $card["title"],
                ];
                $counter++;
            }
        }

        return $cards;

    }
}