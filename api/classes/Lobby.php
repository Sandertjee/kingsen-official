<?php
/**
 * Created by PhpStorm.
 * User: Sander
 * Date: 12-12-2018
 * Time: 12:38
 */

class Lobby {

    private $code;
    private $fileEditor;


    static function getSessionCode(){
        return (isset($_SESSION["lobbyCode"]))? $_SESSION["lobbyCode"]:0;

    }

    static function setSessionCode($value){
        $_SESSION["lobbyCode"] = $value;
    }

    public function __construct() {
        $this->fileEditor = new FileEditor(__DIR__ . "/../../rooms/");
    }

    function newLobby() {
        $rawcards = Card::generateCards();
        $cardids = array_keys($rawcards);
        $lobbyCode = $this->generateRoomCode();
        $this->code = $lobbyCode;

//        var STATE_LOGIN = 0;
//        var STATE_CREATE = 1;
//        var STATE_LOBBY = 2;
//        var STATE_GAME = 3;
//        var STATE_END = 4;

        $generalarray = array('game_state' => 2, 'cards_left' => $cardids, "turn" => 2);

        $this->fileEditor->createfolder($lobbyCode);
        $this->fileEditor->createfile("cards", "json", json_encode($rawcards, JSON_PRETTY_PRINT));
        $this->fileEditor->createfile("general", "json", json_encode($generalarray, JSON_PRETTY_PRINT));
        $this->fileEditor->createfolder("players");
        Lobby::setSessionCode($lobbyCode);
    }

    function getData(){
        $generalFile = $this->fileEditor->getFile("/general", "json");
        $generalFileArray = json_decode($generalFile, true);
        return $generalFileArray;

    }

    function getPlayer(){
        return new Player($this->code);
    }

    function get($lobbyCode){
        $this->fileEditor->getFolder($this->fileEditor->getPath()."/".$lobbyCode."/");
            if($this->fileEditor->folderExists()){
                $this->code = $lobbyCode;
                self::setSessionCode($lobbyCode);
                return true;
            }
            else{
                return false;
            }
    }

    function getPlayers(){
        $files = array_diff(scandir(__DIR__ . "/../../rooms/". $this->code."/players/"), array('..', '.'));


        return json_encode(str_replace(".json","",$files), JSON_PRETTY_PRINT);
//                foreach($files as $file){
//                    echo str_replace(".json","",urldecode($file))."<br />";
//                }
    }

    function getPlayerInfo($lobbyCode, $playerName){
        return $this->fileEditor->getFile($lobbyCode."/players/".urlencode($playerName), "json");
    }

    function readyPlayer($lobbyCode, $playerName){
        $playerFile = json_decode($this->fileEditor->getFile($lobbyCode."/players/".$playerName, "json"), true);
        $playerFile["ready"] = true;

        $newPlayerFile = json_encode($playerFile, JSON_PRETTY_PRINT);
        $this->fileEditor->savefile($lobbyCode."/players/".$playerName, "json", $newPlayerFile);
    }

    function nextTurn(){
        $lobbyFile = $this->getData();

        $players = json_decode($this->getPlayers(), true);

        $playerCount = count($players) + 1;
        $turn = $lobbyFile["turn"];


        if($turn < 2){
            $lobbyFile["turn"] = 2;
        }
        elseif($turn > $playerCount){
            $lobbyFile["turn"] = 2;
        }
        else{
            $lobbyFile["turn"]++;
        }


        $newLobbyFile = json_encode($lobbyFile, JSON_PRETTY_PRINT);

        if($this->fileEditor->savefile("general", "json", $newLobbyFile)){
            return true;
        }
        else{
            return false;
        }
    }


    function nextGameState(){
        $lobbyFile = $this->getData();

        $gameState = $lobbyFile["turn"];
        $gameState++;

        $lobbyFile["game_state"] = $gameState;

        $newLobbyFile = json_encode($lobbyFile, JSON_PRETTY_PRINT);

        if($this->fileEditor->savefile("general", "json", $newLobbyFile)){
            return true;
        }
        else{
            return false;
        }
    }


    function generateRoomCode(){
        $possibilities = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $lobbycode = "";

        for ($i = 0; $i < 5; $i++) {
            // Generate code using the possibilities
            $lobbycode .= $possibilities[rand(0, strlen($possibilities) -1)];
        }

        if(file_exists(__DIR__."/../../rooms/".$lobbycode)){
            return $this->generateRoomCode();
        }
        else{
           return $lobbycode;
        }
    }


    function getCardData($cardID){
        $rawCards = json_decode($this->fileEditor->getFile("/cards", "json"), true);
        foreach($rawCards as $card){
            if($card["id"] === $cardID){
                return $card;
            }
        }
        return null;
    }




























}