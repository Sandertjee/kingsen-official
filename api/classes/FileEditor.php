<?php
/**
 * Created by PhpStorm.
 * User: Sander
 * Date: 12-12-2018
 * Time: 12:35
 */

class FileEditor {

    public $path;

    public function __construct($path) {
        $this->path = $path;
    }

    public function createfolder($name){
        mkdir($this->path.$name);
        $this->path = $this->path.$name."/";

    }

    public function createfile($name, $extension, $contents){
        $newfile = fopen($this->path.$name.".".$extension, 'w');
        fwrite($newfile, $contents);
        fclose($newfile);
    }

    public function getFile($name, $extension){
        try{
            $file = file_get_contents($this->path.$name.".".$extension);
            if($file){
                return $file;
            }
            else{
                return false;
            }
        }
        catch(Exception $e){
            return false;
        }


    }

    public function getfolder($path){
        $this->path = $path;
    }

    public function getPath() {
        return $this->path;
    }

    public function folderExists(){
        if(file_exists($this->path)){
            return true;
        }
        else{
            return false;
        }
    }


    public function savefile($name, $extension, $contents){
        if(file_exists($this->path.$name.".".$extension)) {

            // opens file and prepares name, extension
            $editedfile = fopen($this->path . $name . "." . $extension, 'w');
            // writes contents to the newly created file
            fwrite($editedfile, $contents);
            // closes the file
            fclose($editedfile);
            return true;
        }
        else{
            return false;
        }
    }

    public function deletefile($name){

    }

    public function deletefolder($name){

    }

}