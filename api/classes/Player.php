<?php
/**
 * Created by PhpStorm.
 * User: Sander
 * Date: 12-12-2018
 * Time: 12:37
 */

class Player {
    private $fileEditor;
    private $lobbyCode;

    static function setSessionName($value){
        $_SESSION["playerName"] = $value;
        return true;
    }

    static function getSessionName(){
        return (isset($_SESSION["playerName"]))? $_SESSION["playerName"]:0;
    }

    public function __construct($lobbyCode) {
        $this->lobbyCode = $lobbyCode;
        $this->fileEditor = new FileEditor(__DIR__ . "/../../rooms/". $lobbyCode."/players/");
    }


    function joinLobby($playerName){
        $friendlyURL = urlencode($playerName);
        $_SESSION["playerName"] = $friendlyURL;

        $contentArray = array('player_name' => $playerName, 'ready' => false, 'inventory' => [], 'group_ID' => $friendlyURL, 'events' => []);
        $this->fileEditor->createfile($friendlyURL, "json", json_encode($contentArray, JSON_PRETTY_PRINT));
        if(Player::setSessionName($playerName)){
            return true;
        }
        return false;
    }


    function get($playerName){
        $file = json_decode($this->fileEditor->getFile($playerName, "json"), true);
        return $file;
    }

    function addToInventory($playerName, $cardID){

        if(!empty($cardID)){
            $playerFile = $this->get($playerName);
            $playerFile["inventory"][] = intval($cardID);

            $newPlayerFile = json_encode($playerFile, JSON_PRETTY_PRINT);
            ($this->fileEditor->savefile($playerName, "json", $newPlayerFile));
            return true;
        }
        else{
            return false;
        }



    }

    function getInventory($playerName) {
        $playerFile = $this->get($playerName);

        return $playerFile["inventory"];
    }

}