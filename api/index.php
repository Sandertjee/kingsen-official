<?php


include_once "classes/Player.php";
include_once "classes/Card.php";
include_once "classes/Event.php";
include_once "classes/Lobby.php";
include_once "classes/FileEditor.php";


define("STATE_LOGIN", 0);
define("STATE_CREATE", 1);
define("STATE_LOBBY", 2);
define("STATE_GAME", 3);
define("STATE_END", 4);


session_start();


//if(isset($_SESSION["isplaying"])){
//
//}

/*
 * Type
 * Action
 * Value1
 *
 *
 */

$type = (isset($_GET["type"]))? $_GET["type"]:NULL;
$action = (isset($_GET["action"]))? $_GET["action"]:NULL;
$value = (isset($_GET["value"]))? $_GET["value"]:"";
$value = explode(",", $value);

if($type == NULL || empty($value)){
    $type = "NULL";
    $action = "NULL";
    $value = "NULL";
}

switch($type){

    case "lobby_new":
        $lobby = new Lobby();
        $lobby->newLobby();
        $player = new Player(Lobby::getSessionCode());
        if($player->joinLobby($value[0])){
            echo json_encode(["error" => false, "message" => ""]);
        }
        else{
            echo json_encode(["error" => true, "message" => "Failed to join lobby " . $value[0]]);
        }
        break;

    case "player_join":
        $lobby = new Lobby();
        if($lobby->get($value[0])){
            $player = $lobby->getPlayer();
            $player->joinLobby($value[1]);
        }
        else{
            echo json_encode(["error" => true, "message" => "Lobby niet gevonden!"]);
        }
        break;

    case "player_add_inventory":
        $lobby = new Lobby();
        $player = new Player(Lobby::getSessionCode());
        if($player->addToInventory(player::getSessionName(), $value[0])){
            echo json_encode(["error" => false, ""]);
        }
        else{
            echo json_encode(["error" => true, "message" => "Lobby niet gevonden!"]);
        }

        break;

    case "get_player_info":
        $lobby = new Lobby();
        echo $lobby->getPlayerInfo(Lobby::getSessionCode(), $value[0]);
        break;

    case "lobby_get_players":
        $lobby = new Lobby();
        if($lobby->get(Lobby::getSessionCode())){
            echo $lobby->getPlayers();
        }
        else{
            echo json_encode(["error" => true, "message" => "Lobby niet gevonden!"]);
        }
        break;

    case "lobby_ready_player":
        $lobby = new Lobby();
        $lobby->readyPlayer(Lobby::getSessionCode(), $value[0]);
        break;

    case "card_random":
        $lobby = new Lobby();
        $file = new FileEditor(__DIR__."/../rooms/".Lobby::getSessionCode()."/");
        $cards = json_decode($file->getFile("cards", "json"), true);

        header("Access-Control-Allow-Origin: *");
        header("Content-Type: application/json");
        echo json_encode($cards[array_rand($cards)], JSON_PRETTY_PRINT);
        break;

    case "lobby_next_turn":
        $lobby = new Lobby();
        $lobby->get(lobby::getSessionCode());
        if($lobby->nextTurn()){
            echo json_encode(["error" => false, "message" => ""]);
        }
        else{
            echo json_encode(["error" => true, "message" => "Failed saving file!"]);
        }
        break;


    case "lobby_next_game_state":
        $lobby = new Lobby();
        $lobby->get(lobby::getSessionCode());

        if($lobby->nextGameState()){
            echo json_encode(["error" => false, "message" => ""]);
        }
        else{
            echo json_encode(["error" => true, "message" => "Failed saving file!"]);
        }
        break;

    case "game_get_data":
        $resultarray = [];
        $lobby = new Lobby();
        $player = new Player(Lobby::getSessionCode());

        $gameDataArray = array('game_state' => 0, 'turn' => NULL, 'player_display_name' => Player::getSessionName(), "lobby_code" => Lobby::getSessionCode(), "inventory" => []);

        if($lobby->get(lobby::getSessionCode())){
            $lobbyData = $lobby->getData();

            $playersArray = json_decode($lobby->getPlayers(), true);

            if($lobbyData["turn"] === array_search(player::getSessionName(), $playersArray)){
                $yourTurn = true;
            }
            else{
                $yourTurn = false;
            }

            $inventory = $player->getInventory(Player::getSessionName());
            $newinventory = [];

            foreach($inventory as $cardID){
                $newinventory[] = $lobby->getCardData($cardID);
            }

            $gameDataArray["game_state"] = $lobbyData["game_state"];
            $gameDataArray["turn"] = $yourTurn;
            $gameDataArray["inventory"] = $newinventory;

            echo json_encode($gameDataArray, JSON_PRETTY_PRINT);
        }
        else{
            echo json_encode(["game_state" => 0], JSON_PRETTY_PRINT);
        }
        break;




    case "card_$":
        $card = new Card($action, $value);
        break;

    case "event":
        $event = new Event($action, $value);
        break;

    default:
        echo "<h1>You have no power here. Get out.</h1>";
        break;
}