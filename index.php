<?php
//
//    include_once "api/classes/Lobby.php";

session_start();
//Lobby::setSessionCode("8K6HR");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-LRlmVvLKVApDVGuspQFnRQJjkv0P7/YFrw84YYQtmYG4nK8c+M+NlmYDCv0rKWpG" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,800|Roboto" rel="stylesheet">

    <link rel="stylesheet" href="assets/css/master.css">

    <script src="assets/js/lib/jquery.min.js"></script>
    <script src="assets/js/main.js"></script>

    <title>Kingsen</title>
</head>
<body>
<div id="canvas" class="canvas">
    Loading files
</div>
</html>